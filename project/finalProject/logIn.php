<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>LOG IN</title>
    <link rel="stylesheet/less" type="text/css" href="styles.less">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
  </head>
  <body>
    <header><?php
    session_start();
         require_once './vendor/autoload.php';  //include the twig library.
         $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
         $twig = new Twig_Environment($loader);

        //setup twig
        $template = $twig->load('title.twig.html');
        //call render to replace values in template with ones specified in my array
        //Since the return value is a string, I can echo it.
        echo $template->render(array("Heading" => "M.E.N.C.O."));
        ?></header>
    <nav>
       <?php
          $template1 = $twig->load('menu.twig.html');
          echo $template1->render(array());
        ?>
      </nav>
        <main>
        <fieldset>
        <legend>Log IN</legend>
        <form action="loggedin.php" method="POST">
          <label>Username :</label> <input type="text" name="uid" placeholder="Username/e-mail"><br><br>
          <label>Password : </label> <input type="password" name="pwd" placeholder="password"><br><br>
          <button type="submit" name="submit" class="button" />Log IN</button>
        <a href="signup.php">  <input type="button" value="CREATE NEW ACCOUNT" class="button"></a>
        </form>
    </fieldset>
        </main>
        <footer>
          <?php
            $template1 = $twig->load('footer.twig.html');
            echo $template1->render(array());
          ?>
        </footer>
  </body>
</html>
