<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Menco</title>
    <link rel="stylesheet/less" type="text/css" href="styles.less">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>
<body>
	<header><?php
      session_start();
      if(isset($_SESSION['u_id']))
      {
        echo '<a href="logout.php"><button type="button">Logout</button></a>';
      }
      else{
        echo '<a href="logIn.php"><button type="button">Log IN</button></a>';
      }
      ?>
      <?php

       require_once './vendor/autoload.php';  //include the twig library.
       $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
       $twig = new Twig_Environment($loader);

       $template = $twig->load('title.twig.html');
       //call render to replace values in template with ones specified in my array
       //Since the return value is a string, I can echo it.
       echo $template->render(array("Heading" => "M.E.N.C.O."));

  if(isset($_SESSION['u_id'])){
        $template = $twig->load('loggedin.twig.html');
        $name = $_SESSION['u_first'];
        echo $template->render(array("Name" => "$name"));
    }
      ?>
</header>
		<nav>
      <?php
      $template1 = $twig->load('menu.twig.html');
      echo $template1->render(array());
      ?>
      </nav>
		<main>
      <?php
        $serverName = "localhost";
        $userName = "CPSC2030";
        $password = "CPSC2030";
        $database = "menco";

        $conn = mysqli_connect($serverName,$userName,$password,$database);
        if (mysqli_connect_errno($conn)){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $row=array();
        $number=mysqli_real_escape_string($conn,$_GET['number']);
        $query = "CALL getonemobile(\"$number\")";
        $res = mysqli_query($conn,$query);
      			 while($fetch = mysqli_fetch_assoc($res)){
      			      $row = $fetch;
      			?>
      			<center><table>
      				  	<tr><td><img src="Project images\phones\<?php echo $row['image1']; ?>" alt="laptops" width="40%"></td><td><img src="Project images\phones\<?php echo $row['image2']; ?>" width="40%" alt="laptops"></td></tr>
                  <tr><td>Name</td><td><?php echo $row['Name']; ?></td></tr>
      				 		<tr><td>Display</td><td><?php echo $row['display']; ?></td></tr>
                  <tr><td>Front Camera</td><td><?php echo $row['front_camera']; ?></td></tr>
                  <tr><td>Processor</td><td><?php echo $row['processor']; ?></td></tr>
                  <tr><td>RAM</td><td><?php echo $row['ram']; ?></td></tr>
                  <tr><td>OS</td><td><?php echo $row['os']; ?></td></tr>
                  <tr><td>Rear Camera</td><td><?php echo $row['rear_camera']; ?></td></tr>
                  <tr><td>Battery</td><td>$ <?php echo $row['battery']; ?></td></tr>
                  <tr><td>Price</td><td><?php echo $row['Price']; ?></td></tr>
                  <tr><td>Description</td><td><?php echo $row['description']; ?></td></tr>
      		   </table></center>
      		 <?php } ?>
          <a href="laptop.php"><button type="button" class="link-btn">Go back to All Laptops</button>
          <a href="buy.php?number=<?php echo $row['SrNo']; ?>&data=mobile"><button type="button" class="link-btn">Buy Now!</button>
		</main>
    <footer>
      <?php
        $template1 = $twig->load('footer.twig.html');
        echo $template1->render(array());
      ?>
    </footer>
</body>
</html>
