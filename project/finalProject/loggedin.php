<?php

session_start();
  if(isset($_POST['submit'])){
    include 'connect.php';
    $uid=mysqli_real_escape_string($conn,$_POST['uid']);
    $pwd=mysqli_real_escape_string($conn,$_POST['pwd']);

    if(empty($uid) || empty($pwd)){
      header("Location: index.php?login=empty");
      exit();
    }else{
      $sql="CALL getUser(\"$uid\")";
      $result = mysqli_query($conn, $sql);
      $check = mysqli_num_rows($result);

      if($check < 1){
        header("Location: index.php?login=error2");
        exit();
      }else{
        if( $row = mysqli_fetch_assoc($result)){
          //de-hashing PASSWORD
          $hashedPwdCheck = password_verify($pwd, $row['user_password']);
          if ($hashedPwdCheck == false){
            header("Location: index.php?login=error3");
            exit();
          }elseif($hashedPwdCheck == true){
            //log in the user here
            $_SESSION['u_id'] = $row['user_id'];
            $_SESSION['u_first'] = $row['user_first'];
            $_SESSION['u_last'] = $row['user_last'];
            $_SESSION['u_email'] = $row['user_email'];
            $_SESSION['u_uid'] = $row['user_uid'];
            header("Location: index.php?login=success");
            exit();
          }
        }
      }
    }
  }else{
    header("Location: index.php?login=emptyfield");
    exit();
  }
?>
