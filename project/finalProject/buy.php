<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Menco</title>
    <link rel="stylesheet/less" type="text/css" href="styles.less">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"> </script>
    <script src="jquey.js" defer></script>
</head>
<body>
	<header><?php
      session_start();
      $user = 'CPSC2030';
      $pwd = 'CPSC2030';
      $server = 'localhost';
      $dbname = 'menco';

      $conn = new mysqli($server, $user, $pwd, $dbname);
      if(isset($_SESSION['u_id']))
      {
        echo '<a href="logout.php"><button type="button">Logout</button></a>';
      }
      else{
        echo '<a href="logIn.php"><button type="button">Log IN</button></a>';
      }
      ?>
      <?php

       require_once './vendor/autoload.php';  //include the twig library.
       $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
       $twig = new Twig_Environment($loader);

       $template = $twig->load('title.twig.html');
       //call render to replace values in template with ones specified in my array
       //Since the return value is a string, I can echo it.
       echo $template->render(array("Heading" => "M.E.N.C.O."));

  if(isset($_SESSION['u_id'])){
        $template = $twig->load('loggedin.twig.html');
        $name = $_SESSION['u_first'];
        echo $template->render(array("Name" => "$name"));
    }
    $number = mysqli_real_escape_string($conn,$_GET['number']);      // S No. of the item which user wants to buy.
    $data = mysqli_real_escape_string($conn,$_GET['data']);
  	$row = array();
    if($data == "laptop"){
    	$qry = "CALL getoneLaptop(\"$number\")";
    	$res = mysqli_query($conn,$qry);
      while($fetch = mysqli_fetch_assoc($res)){
					$row = $fetch;
      }
    }
    else{
    	$query = "CALL getonemobile(\"$number\")";
    	$res = mysqli_query($conn,$query);
      while($fetch = mysqli_fetch_assoc($res)){
					$row = $fetch;
      }
    }


      ?>
</header>
		<nav>
      <?php
      $template1 = $twig->load('menu.twig.html');
      echo $template1->render(array());
      ?>
      </nav>
		<main>
      <center><h2>Hii <?php echo $name?> We need your following information in order to Deliver this product to you.</h2>
      <h2>CASH ON DELIVERY ONLY !!!</h2></center>
      <center><p>Name Of the Item : <?php echo $row['Name']?></p>
      <p id="pricewotax"><?php echo $row['Price']?></p>
      <p id="pricewtax"></p></center>
      <fieldset>
      <legend>Sign UP</legend>
      <form action="bought.php" method="POST">
        <label>Name :</label> <input type="text" name="namebuy" id="namebuy" placeholder="<?php echo $name?>"><br><br>
        <label>Address :</label> <input type="text" name="addressbuy" id="addressbuy" placeholder="Address"><br><br>
        <label>Phone Number : </label> <input type="text" name="numberbuy" id="numberbuy" placeholder="Phone Number"><br><br>
        <label>Postal Code :</label> <input type="text" name="codebuy" id="codebuy" placeholder="Postal Code"><br><br>
        <button type="submit" name="submit" class="button" />Buy Now</button>
      </form>
  </fieldset>

		</main>
    <footer>
      <?php
        $template1 = $twig->load('footer.twig.html');
        echo $template1->render(array());
      ?>
    </footer>
</body>
</html>
