<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CONTACT US</title>
    <link rel="stylesheet/less" type="text/css" href="styles.less">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
  </head>
  <body>
  <header>
    <?php
    session_start();
    if(isset($_SESSION['u_id']))
    {
      echo '<a href="logout.php"><button type="button">Logout</button></a>';
    }
    else{
      echo '<a href="logIn.php"><button type="button">Log IN</button></a>';
    }
    ?>
    <?php
       require_once './vendor/autoload.php';  //include the twig library.
       $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
       $twig = new Twig_Environment($loader);

      //setup twig
      $template = $twig->load('title.twig.html');
      //call render to replace values in template with ones specified in my array
      //Since the return value is a string, I can echo it.
      echo $template->render(array("Heading" => "M.E.N.C.O."));
      if(isset($_SESSION['u_id'])){
            $template = $twig->load('loggedin.twig.html');
            $name = $_SESSION['u_first'];
            echo $template->render(array("Name" => "$name"));
        }
      ?></header>
    <nav>
      <?php
          $template1 = $twig->load('menu.twig.html');
          echo $template1->render(array());
      ?>
      </nav>
        <main>
    <center>
        <table>
          <tr>
            <th>Customer-Care</th><td>1800-5689-000</td>
          </tr>
          <tr>
            <th>Email-Address</th><td>menco362@gmail.com</td>
          </tr>
          <tr>
            <th>Address</th><td>7391 Knight Street, Seattle , Washington</td>
          </tr>
        </table>
        <br>
        <h2>Any questions Contact us using Above information ;)</h2></center>
        <footer>
          <?php
            $template1 = $twig->load('footer.twig.html');
            echo $template1->render(array());
          ?>
        </footer>
      </main>
  </body>
</html>
