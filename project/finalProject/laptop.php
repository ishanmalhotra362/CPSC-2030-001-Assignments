<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Menco</title>
    <link rel="stylesheet/less" type="text/css" href="styles.less">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>
<body>
	<header><?php
      session_start();
      if(isset($_SESSION['u_id']))
      {
        echo '<a href="logout.php"><button type="button">Logout</button></a>';
      }
      else{
        echo '<a href="logIn.php"><button type="button">Log IN</button></a>';
      }
      ?>
      <?php

       require_once './vendor/autoload.php';  //include the twig library.
       $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
       $twig = new Twig_Environment($loader);

       $template = $twig->load('title.twig.html');
       //call render to replace values in template with ones specified in my array
       //Since the return value is a string, I can echo it.
       echo $template->render(array("Heading" => "M.E.N.C.O."));

  if(isset($_SESSION['u_id'])){
        $template = $twig->load('loggedin.twig.html');
        $name = $_SESSION['u_first'];
        echo $template->render(array("Name" => "$name"));
    }
      ?>
</header>
		<nav>
      <?php
      $template1 = $twig->load('menu.twig.html');
      echo $template1->render(array());
      ?>
      </nav>
		<main>
      <?php
        $serverName = "localhost";
        $userName = "CPSC2030";
        $password = "CPSC2030";
        $database = "menco";

        $conn = mysqli_connect($serverName,$userName,$password,$database);
        if (mysqli_connect_errno($conn)){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $row=array();
        $query = 'CALL getAllLaptop';
        $res = mysqli_query($conn,$query);
      			 while($fetch = mysqli_fetch_assoc($res)){
      			      $row = $fetch;
      			?>
      			<div class="card">
      				 <div class="cont">
      				  	<img src="Project images\laptops\<?php echo $row['image1']; ?>" alt="laptops">
                  <p><?php echo $row['Name']; ?><br>$ <?php echo $row['Price']; ?></p>
      				 		<a href="buy.php?number=<?php echo $row['SNo']; ?>&data=laptop"><button type="button" class="link-btn">Buy Now</button></a>
                  <a href="lapDesc.php?number=<?php echo $row['SNo']; ?>"><button type="button" class="link-btn">Description</button></a>
      				 </div>
      			 </div>
      		 <?php } ?>
		</main>
    <footer>
      <?php
        $template1 = $twig->load('footer.twig.html');
        echo $template1->render(array());
      ?>
    </footer>
</body>
</html>
