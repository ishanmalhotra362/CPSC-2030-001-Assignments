﻿<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Menco</title>
    <link rel="stylesheet/less" type="text/css" href="styles.less">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>
<body>
	<header><?php
      session_start();
      if(isset($_SESSION['u_id']))
      {
        echo '<a href="logout.php"><button type="button">Logout</button></a>';
      }
      else{
        echo '<a href="logIn.php"><button type="button">Log IN</button></a>';
      }
      ?>
      <?php

       require_once './vendor/autoload.php';  //include the twig library.
       $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
       $twig = new Twig_Environment($loader);

       $template = $twig->load('title.twig.html');
       //call render to replace values in template with ones specified in my array
       //Since the return value is a string, I can echo it.
       echo $template->render(array("Heading" => "M.E.N.C.O."));

  if(isset($_SESSION['u_id'])){
        $template = $twig->load('loggedin.twig.html');
        $name = $_SESSION['u_first'];
        echo $template->render(array("Name" => "$name"));
    }
      ?>
</header>
		<nav>
      <?php
      $template1 = $twig->load('menu.twig.html');
      echo $template1->render(array());
      ?>
      </nav>
		<main>
			<img src="anim.gif" alt="gadget">
			<p>Information technology is concerned with improvements in a variety of human and organizational
        problem-solving endeavors, through the design, development and use of technologically based systems
        and processes that enhance the efficiency and effectiveness of information in a variety of strategic,
        tactical and operational situations. Our website is the platform where you can buy almost every technical
        products from diffrent companies.</p>
		</main>
    <footer>
      <?php
        $template1 = $twig->load('footer.twig.html');
        echo $template1->render(array());
      ?>
    </footer>
</body>
</html>
