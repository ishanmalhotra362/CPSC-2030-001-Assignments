DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `allMsgs`()
select server_time,user_name,message from chat order by id desc$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `latest`()
select server_time,user_name,message from chat where server_time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) limit 10$$
DELIMITER ;
