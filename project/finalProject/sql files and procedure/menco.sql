-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 07:58 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `menco`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllLaptop` ()  SELECT * from laptop$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllmobile` ()  SELECT * FROM mobile$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getoneLaptop` (IN `number` VARCHAR(255))  Select * from laptop where laptop.SNo=number$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getonemobile` (IN `number` VARCHAR(255))  SELECT * FROM mobile where mobile.srNo=number$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bought`
--

CREATE TABLE `bought` (
  `SNo` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `Postal Code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laptop`
--

CREATE TABLE `laptop` (
  `SNo` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Size` varchar(255) NOT NULL,
  `Color` varchar(255) NOT NULL,
  `Processor` varchar(255) NOT NULL,
  `Memory` varchar(255) NOT NULL,
  `Ram` varchar(255) NOT NULL,
  `Windows` varchar(255) NOT NULL,
  `Price` float NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laptop`
--

INSERT INTO `laptop` (`SNo`, `Name`, `Size`, `Color`, `Processor`, `Memory`, `Ram`, `Windows`, `Price`, `image1`, `image2`, `Description`) VALUES
(1, 'ASUS Vivobook', '15.6', 'Grey', 'Intel Core i5-8250U', '256GB SSD', '8GB RAM', 'Windows 10', 699.99, '1_1.jpg', '1_2.jpg', 'Enjoy fast and reliable computing everywhere you go with the ASUS Vivobook X510UA-BB51-CB 15.6 laptop. Extremely thin and lightweight, this laptop was made for high performance on-the-go, and the 1.6GHz Intel Core processor, 8GB of RAM, 256GB solid state '),
(2, 'Razer Blade 15 RZ09-02705E76-MSU1', '15.6', 'Black', 'Intel Core i7-8750H', '1TB HDD + 128GB SSD', '16GB memory', 'Windows 10', 2079, '2_1.jpg', '2_2.jpg', 'Slice up your gaming competition with the Razer Blade 15, the world\'s smallest 15.6-inch gaming laptop. the action comes fast and smooth with VR-ready NVIDIA GeForce GTX graphics, while an 8th Gen Intel Core processor and loads of memory pack a thunderous'),
(3, 'Dell Inspiron 13 i5379-7302', '13.3', 'Grey', 'Intel Core i7-8550U', '256GB SSD', '8GB memory', 'Windows 10', 849, '3_1.jpg', '3_2.jpg', 'Boasting an innovative 360-degree hinge, the Dell Inspiron 13 bends over backwards to please with four different modes: tent, stand, laptop, and tablet. Its 13.3-inch Full HD wide-angle display features more screen and less bezel, so you have plenty of ro'),
(4, 'Surface Pro 6', '12.3', 'Black', 'i5-8250U or i7-8650U', '128GB, 256GB, 512GB, or 1TB', '8GB or 16GB RAM', 'Windows 10', 1179, '4_1.jpg', '4_2.jpg', 'Ultra-light and versatile. Get productive your way with new Surface Pro 6 — now with the latest 8th Gen Intel® Core™ processor.'),
(5, 'Lenovo Yoga C930 81C4007HUS 2 in 1 PC', '13.9', 'Black', 'Intel Core i7-8550H ', '256GB SSD', '12GB memory', 'Windows 10', 1899, '5_1.jpg', '5_2.jpg', 'Seductively slim and impressively versatile, the Lenovo Yoga C930 brings a new level of convenience to your day. This flexible 2 in 1 is power-packed with an 8th Gen Intel processor, 12GB of memory, and an enduring 12-hour battery life*. Plus, it sports a'),
(6, 'Dell Inspiron 13 i5379-5296', '13.3', 'Grey', 'Intel Core i5-8250U', '1TB HDD', '8GB memory', 'Windows 10', 1079, '6_1.jpg', '6_2.jpg', 'Boasting an innovative 360-degree hinge, the Dell Inspiron 13 bends over backwards to please with four different modes: tent, stand, laptop, and tablet. Its 13.3-inch Full HD wide-angle display features more screen and less bezel, so you have plenty of ro'),
(7, 'ASUS VivoBook Flip TP401MA', '14', 'Grey', 'Intel Pentium Silver N5000', '64GB eMMC', '4GB memory', 'Windows 10', 349, '7_1.jpg', '7_2.jpg', 'Seductively slim and super-flexible, the ASUS VivoBook Flip 14 brings a new level of versatility to your work and play. Its 360° hinge spins the 14-inch Full HD touchscreen all the way around for use in a variety of poses, and its compact frame and low we'),
(8, 'Huawei MateBook D', '14', 'Grey', 'Intel Core i7-8550U', '512GB SSD', '8GB memory', 'Windows 10', 1129, '8_1.jpg', '8_2.jpg', 'Light, elegant, and portable, the Huawei MateBook D is in a business class of its own. Powered with an 8th Gen Intel processor and snappy NVIDIA GeForce graphics, it’s a master of multitasking. The durable aluminum body features a matte finish for polishe'),
(9, 'Microsoft Surface Pro Core M', '12.3', 'Black', 'Intel® Core™ 7th-generation i7', '1TB SSD', '16GB RAM', 'Windows 10', 1800, '9_1.jpg', '9_2.jpg', 'Better than ever, the new Surface Pro (5th Gen) gives you a best-in-class laptop, plus the versatility of a studio and tablet. The stunning PixelSense Display supports Surface Pen1 and touch, while up to 13.5 hours of battery life2 gives you plenty of jui'),
(10, 'HP Pavilion x360', '14', 'Silver', 'Intel Core i5-8250U', '256GB SSD', '8GB RAM', 'Windows 10', 999.99, '10_1.jpg', '10_2.jpg', 'With plenty of power and versatility, you\'ll never need another device after owning the HP 14 touchscreen 2-in-1 laptop. Get all of your productivity tasks accomplished quickly a 1.65GHz processor and advanced graphics technology. Take your work on the go'),
(11, 'ACER Swift 3', '14', 'Grey', 'Intel Core i5 8250H Processor', '128GB Ssd', '8GB', 'Windows 10', 798, '11_1.jpg', '11_2.jpg', 'The Acer Swift 3 meets the demands of today’s modern, on-the-go lifestyles—thanks to its super-fast connection speeds, long battery life and unmatched portability. This PC provides a fast, smooth, and amazingly responsive computing experience without comp'),
(12, 'Dell 15.6 Laptop', '15.6', 'Platinum Silver', 'Intel Core i5-8250U', '256GB SSD', '8GB RAM', 'Windows 10', 799.99, '12_1.jpg', '12_2.jpg', 'This Dell laptop delivers a powerful, efficient performance with its 1.60GHz i5 processor, 256GB SSD, & 8GB of RAM that comfortably handle your daily demands, whether you\'re web browsing or multitasking. With its Full HD display, you\'ll also enjoy watchin'),
(13, 'HP 14 Laptop', '14', 'Natural Silver/Ash Silver', 'Intel Core i5-8250U', '256GB SSD', '8GB RAM', 'Windows 10', 899.99, '13_1.jpg', '13_2.jpg', 'Bring reliable performance and maximum portability to your computing needs with the HP 14 laptop. It boasts an 8th generation Intel Core i5-8250U processor with 8GB DDR4 SDRAM for the speed and functionality you need. It\'s preinstalled with Windows 10 for'),
(14, 'HP 15.6 Touchscreen Laptop', '15.6', 'Natural Silver', 'Intel Core i5-8250U', '1TB HDD', '8GB RAM', 'Windows 10', 799.99, '14_1.jpg', '14_2.jpg', 'Enjoy reliable computing performance with this 15.6 HP laptop, which features a 1.6GHz Intel Core i5 processor and 8GB of DDR4 RAM. A 1TB hard drive gives you tons of digital storage space, while Bluetooth and 3 USB ports give you lots of freedom to expan'),
(15, 'Apple MacBook Pro', '13.3', 'Silver', 'Intel Core i5 2.3GHz', '256GB SSD ', '8GB RAM', 'Mac OS X', 1979.99, '15_1.jpg', '15_2.jpg', 'The new MacBook Pro is razor-thin, featherlight, and now even faster and more powerful than before. It has the brightest, most colourful Mac notebook display. And it features up to 10 hours of battery life. It\'s a notebook built for the work you do every '),
(16, 'Apple MacBook Air', '13.3', 'Silver', 'Intel Core i5 1.8 GHz', '128GB SSD', '8GB RAM', 'Mac OS X', 1199.99, '16_1.jpg', '16_2.jpg', 'The 13-inch MacBook Air features 8GB of memory, a fifth-generation Intel Core processor, Thunderbolt 2, great built-in apps and all-day battery life.* It’s thin, light and durable enough to take everywhere you go -- and powerful enough to do everything on'),
(17, 'Apple MacBook Pro', '13.3', 'Space Grey', 'Intel Core i5 2.3GHz', '128GB SSD ', '8GB RAM', 'Mac OS X', 1729.99, '17_1.jpg', '17_2.jpg', 'The new MacBook Pro is razor-thin, featherlight, and now even faster and more powerful than before. It has the brightest, most colourful Mac notebook display. And it features up to 10 hours of battery life.* It\'s a notebook built for the work you do every'),
(18, 'Dell G7 15.6\' Gaming Laptop', '15.6', 'Black', 'Intel Core i7-8750H', '1TB HDD/128GB SSD', '16GB RAM', 'NVIDIA GeForce GTX 1060/Win 10', 1699.99, '18_1.jpg', '18_2.jpg', 'Designed to deliver unparallelled performance, the Dell G7 15.6 gaming laptop features an 8th generation hex-core i7 processor, 16GB of RAM, and a NVIDIA GeForce GTX 1060 card with 6GB of dedicated video memory. Its innovative Max-Q design standard means '),
(19, 'Acer Nitro 15.6 Gaming Laptop', '15.6', 'Black-Orange', 'Intel Core i5-7300HQ', '256GB SSD', '8GB RAM', 'NVIDIA GTX 1050Ti/Win 10', 1149.99, '19_1.jpg', '19_2.jpg', 'Whether you\'re entering the arena or the battlefield, Acer\'s 15.6 Nitro 5 will help keep you in command of the action. Boasting a 2.5GHz Intel Core i5 processor with 8GB RAM, NVIDIA GTX 1050Ti graphics, and Acer\'s Coolboost technology, Nitro 5 runs the th'),
(20, 'ASUS TUF 15.6 Gaming Laptop', '15.6', 'Red', 'Intel Core i5-8300H', '1TB HDD', '8GB RAM', 'NVIDIA GTX 1050/Windows 10', 1099.99, '20_1.jpg', '20_2.jpg', 'The ASUS TUF 15.6 laptop is a mobile powerhouse for serious gamers. It features an 8th generation Intel Core i5-8300H processor, 8GB of RAM, GeForce GTX 1050 graphics, Windows 10 for powerful, intuitive performance every time you log in. Boasting a slim 0');

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE `mobile` (
  `srNo` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `display` varchar(255) NOT NULL,
  `processor` varchar(255) NOT NULL,
  `front_camera` varchar(255) NOT NULL,
  `ram` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `rear_camera` varchar(255) NOT NULL,
  `battery` varchar(255) NOT NULL,
  `Price` float NOT NULL,
  `description` varchar(255) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`srNo`, `Name`, `display`, `processor`, `front_camera`, `ram`, `os`, `rear_camera`, `battery`, `Price`, `description`, `image1`, `image2`) VALUES
(1, 'Honor 8X', '1080x2340px', 'octa_core', '16mega px', '4gb', 'Android 8.1 Oreo', '20mega px', '3750mAh', 214.1, 'Honor 8X smartphone was launched in September 2018. The phone comes with a 6.50_inch touchscreen display with a resolution of 1080 pixels by 2340 pixels at a PPI of 396 pixels per inch', 'h1_1.jpg', 'h1_2.jpg'),
(2, 'Samsung Galaxy S9', '1440x2960px', 'octa_core', '8mega px', '4gb', 'Android 8.0', '12mega px', '3000mAh', 670.89, 'Samsung Galaxy S9 smartphone was launched in March 2018. The phone comes with a 5.80_inch touchscreen display with a resolution of 1440 pixels by 2960 pixels at a PPI of 568 pixels per inch.', 'h2_1.jpg', 'h2_2.jpg'),
(3, 'iphone XR', '828x1792px', 'hexa_core', '7mega px', '64gb', 'ios 12', '12mega px', '', 997, 'Apple iPhone XR smartphone was launched in September 2018. The phone comes with a 6.10_inch touchscreen display with a resolution of 828 pixels by 1792 pixels at a PPI of 326 pixels per inch', 'h3_1.jpg', 'h3_2.jpg'),
(4, 'Samsung Galaxy Note 9', '1440x2960px', 'octa_core', '8mega px', '6gb', 'Android 8.0', '12mega px', '4000mAh', 969, 'Samsung Galaxy Note 9 smartphone was launched in August 2018. The phone comes with a 6.40_inch touchscreen display with a resolution of 1440 pixels by 2960 pixels.', 'h4_1.jpg', 'h4_2.jpg'),
(5, 'Google Pixel 2 XL', '1440x2880px', 'octa_core', '8mega px', '4gb', 'Android 8.1', '12mega px', '3520mAh', 548, 'Google Pixel 2 XL smartphone was launched in November 2017. The phone comes with a 6.00_inch touchscreen display with a resolution of 1440 pixels by 2880 pixels at a PPI of 538 pixels per inch', 'h5_1.jpg', 'h5_2.jpg'),
(6, 'Huawei Mate 20 Pro', '1440x3120px', 'octa_core', '24mega px', '6gb', 'Android 9.0', '40mega px', '4200mAh', 548, 'Huawei Mate 20 Pro smartphone was launched in October 2018. The phone comes with a 6.39_inch touchscreen display with a resolution of 1440 pixels by 3120 pixels', 'h6_1.jpg', 'h6_2.jpg'),
(7, 'LG G7 One', '1440x3120px', 'octa_core', '8mega px', '4gb', 'Android 8.1 oreo', '16mega px', '3000mAh', 219, 'LG G7 One smartphone was launched in August 2018. The phone comes with a 6.10_inch touchscreen display with a resolution of 1440 pixels by 3120 pixels', 'h7_1.jpg', 'h7_2.jpg'),
(8, 'Google Pixel 3', '1080x2160px', 'octa_core', '8mega px', '4gb', 'Android 9.0', '12mega px', '2915mAh', 912, 'Google Pixel 3 smartphone was launched in October 2018. The phone comes with a 5.50_inch touchscreen display with a resolution of 1080 pixels by 2160 pixels at a PPI of 443 pixels per inch', 'h8_1.jpg', 'h8_2.jpg'),
(9, 'Apple iPhone XS Max', '1242x2688px', 'hexa_core', '7mega px', '64gb', 'ios 12', '12mega px', '', 1414, 'The Apple iPhone XS Max is powered by hexa_core processor. The phone packs 64GB of internal storage that cannot be expanded. As far as the cameras are concerned, the Apple iPhone XS Max packs a 12_megapixel', 'h9_1.jpg', 'h9_2.jpg'),
(10, 'Micromax Canvas 5 Lite', '720x1280px', 'quad_core', '5mega px', '2gb', 'Android 5.1', '8mega px', '2000mAh', 940, 'The Micromax Canvas 5 Lite is powered by 1.3GHz quad_core processor and it comes with 2GB of RAM. The phone packs 16GB of internal storage that cannot be expanded', 'h10_1.jpg', 'h10_2.jpg'),
(11, 'Moto E5 Plus', '720x1440px', 'octa_core', '5mega px', '3gb', 'Android 8.0', '12mega px', '5000mAh', 121.93, 'The Moto E5 Plus is powered by 1.4GHz octa_core processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 128GB via a microSD card.', 'h11_1.jpg', 'h11_2.jpg'),
(12, 'Xiaomi Mi A2', '1080x2160px', 'octa_core', '20mega px', '4gb', 'Android 8.1', '12mega px', '3000mAh', 228, 'The Xiaomi Mi A2 is powered by octa_core processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that cannot be expanded', 'h12_1.jpg', 'h12_2.jpg'),
(13, 'OnePlus 6', '1080x2280px', 'octa_core', '16mega px', '8gb', 'Android 8.1', '16mega px', '3300mAh', 499, 'The OnePlus 6 is powered by 2.8GHz octa_core processor and it comes with 8GB of RAM. The phone packs 128GB of internal storage that cannot be expanded', 'h13_1.jpg', 'h13_2.jpg'),
(14, 'Xiaomi Redmi 6A', '720x1440px', 'quad_core', '5mega px', '2gb', 'Android 8.1', '13mega px', '3000mAh', 94, 'The Xiaomi Redmi 6A is powered by 2GHz quad_core (4x2GHz) processor and it comes with 2GB of RAM. The phone packs 16GB of internal storage that can be expanded up to 256GB via a microSD card', 'h14_1.jpg', 'h14_2.jpg'),
(15, 'Realme U1', '1080x2340px', 'quad_core', '25mega px', '4gb', 'Android 8.1', '13mega px', '3500mAh', 185, 'Realme U1 smartphone was launched in November 2018. The phone comes with a 6.30_inch touchscreen display with a resolution of 1080 pixels by 2340 pixels at a PPI of 409 pixels per inch', 'h15_1.jpg', 'h15_2.jpg'),
(16, 'LG G6', '1440x2880px', 'Qualcomm Snapdragon', '5mega px', '32gb', 'Android 7.0', '13mega px', '3300mAh', 383, 'LG G6 smartphone was launched in February 2017. The phone comes with a 5.70_inch touchscreen display with a resolution of 1440 pixels by 2880 pixels at a PPI of 564 pixels per inch', 'h16_1.jpg', 'h16_2.jpg'),
(17, 'Redmi Note 5 Pro', '1080x2160px', 'octa_core', '20mega px', '4gb', 'Android 7.1.1', '12mega px', '4000mAh', 186, 'The Redmi Note 5 Pro is powered by 1.8GHz octa_core processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that can be expanded up to 128GB via a microSD card', 'h17_1.jpg', 'h17_2.jpg'),
(18, 'Oppo A7', '720x1520px', 'Qualcomm Snapdragon', '16mega px', '4gb', 'Android 8.1', '16mega px', '4230mAh', 221, 'Oppo A7 comes with 4GB of RAM. The phone packs 64GB of internal storage that can be expanded up to 256GB via a microSD card and a 16_megapixel front shooter for selfies', 'h18_1.jpg', 'h18_2.jpg'),
(20, 'Vivo V7', '1080x1920px', 'octa_core', '24mega px', '4gb', 'Android 7.1', '16mega px', '3000mAh', 242, 'The Vivo V7 is powered by 1.8GHz octa_core processor and it comes with 4GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card', 'h20_1.jpg', 'h20_2.jpg'),
(19, 'Asus ZenFone 3', '1080x1920px', 'octa_core', '8mega px', '4gb', 'Android 6.0', '16mega px', '3000mAh', 185, 'The Asus ZenFone 3 is powered by a 3000mAh non removable battery. It measures 152.50 x 77.30 x 7.60 and weighs 155.00 grams', 'h19_1.jpg', 'h19_2.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bought`
--
ALTER TABLE `bought`
  ADD PRIMARY KEY (`SNo`);

--
-- Indexes for table `laptop`
--
ALTER TABLE `laptop`
  ADD PRIMARY KEY (`SNo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bought`
--
ALTER TABLE `bought`
  MODIFY `SNo` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
