-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 07:56 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajax`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `allMsgs` ()  select server_time,user_name,message from chat order by id desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `latest` ()  select server_time,user_name,message from chat where server_time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) limit 10$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `Id` int(11) NOT NULL,
  `server_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_name` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`Id`, `server_time`, `user_name`, `message`) VALUES
(20, '2018-11-25 16:08:28', 'binny', 'hello to all'),
(21, '2018-11-25 16:08:35', 'ruchi', 'heyyy'),
(22, '2018-11-25 16:08:41', 'fbhdfhdf', 'hdbhf'),
(23, '2018-11-25 18:27:37', 'Ishu', 'hello'),
(24, '2018-11-25 18:37:59', 'ishan', 'lol'),
(25, '2018-11-25 18:38:44', 'ishan', 'lol'),
(26, '2018-11-25 18:38:49', 'ishan', 'lol'),
(27, '2018-11-25 21:53:13', 'Ishu', 'hello'),
(28, '2018-11-25 22:00:59', 'yoooo', 'buddy'),
(29, '2018-11-25 22:07:30', 'heeeeeeeeeeeeellllooooo', 'jyghgffg'),
(30, '2018-11-25 23:04:56', 'hello vhfbjbdc', 'YUUP'),
(31, '2018-11-26 01:54:24', 'ishu', 'yoooo'),
(32, '2018-12-03 18:18:12', 'hello', 'yoooooo'),
(33, '2018-12-03 18:42:59', 'ishu', 'hello');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
