<?php
session_start();
/*
reference: https://www.youtube.com/watch?v=xb8aad4MRx8
*/

if(isset($_POST['submit'])){
  include_once 'connect.php';
  $first = mysqli_real_escape_string($conn, $_POST['first']);
  $last = mysqli_real_escape_string($conn, $_POST['last']);
  $email = mysqli_real_escape_string($conn, $_POST['email']);
  $uid = mysqli_real_escape_string($conn, $_POST['uid']);
  $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);

  if(empty($first) || empty($last) || empty($email) || empty($uid) || empty($pwd)){
    header("Location: signup.php?signup=empty");
    exit();
  }else{
    if (!preg_match("/^[a-zA-Z]*$/", $first) || !preg_match("/^[a-zA-Z]*$/", $last)){
      header("Location: signup.php?signup=invalid");
      exit();
    }else{
      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        header("Location: signup.php?signup=email");
        exit();
      }else{
        $sql = "SELECT * FROM user WHERE user_uid='$uid'";
        $result = mysqli_query($conn, $sql);
        $Check = mysqli_num_rows($result);

        if($Check > 0){
          header("Location: signup.php?signup=usertaken");
          exit();
        }else{
          //HASING THE PASSWORD
          $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);
          //INSERT THE user into SQL into Database
          $sql = "INSERT INTO users(user_first,user_last,user_email,user_uid,user_password) VALUES ('$first', '$last', '$email', '$uid', '$hashedPwd');";
          $result = mysqli_query($conn, $sql);
          header("Location: signup.php?signup=success");
          exit();
        }
      }
    }
  }
}
else{
  header("Location: signup.php");
  exit();
}

?>
