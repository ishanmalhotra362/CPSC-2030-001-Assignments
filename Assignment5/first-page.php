<?php
	$serverName = "localhost";
	$userName = "CPSC2030";
	$password = "CPSC2030";
	$database = "pokedex";

	$conn = mysqli_connect($serverName,$userName,$password,$database);
	if (mysqli_connect_errno($conn)){
  		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

if(isset($_GET['name'])){
	$type = mysqli_real_escape_string($conn,$_GET['name']);
	$row = array();
	$qry = "CALL getTypeData(\"$type\")";
	$res = mysqli_query($conn,$qry);
	if(mysqli_num_rows($res)>0){

	}
}
else{
	$row = array();
	$query = 'CALL getAllData';
	$res = mysqli_query($conn,$query);
	if(mysqli_num_rows($res)>0){

  }
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>My first Home page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet/less" type="text/css" href="style.less">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>

</head>
	<body>
			<div class="header">
				<img src="images/poke.png" alt="pokemon">
				<h1><i>Pokemon</i></h1>
				<a href="first-page.php"><button type="button" class="right">Back to first Page</button></a>
			</div>
		<div class="container">
			<h3>Here are the following total Pokemon lists.</h3>
			<?php
			 		while($fetch = mysqli_fetch_assoc($res)){
					$row = $fetch;
			?>
			<div class="card">
				 <div class="cont">
				  	<h2>Name:<?php echo $row['name']; ?></h2>
				 		<h4>Pokemon Type:<a href="first-page.php?name=<?php echo $row['type']; ?>"><?php echo $row['type']; ?></a></h4>
				 		<h4>Pokemon number:<?php echo $row['Nat']; ?></h4>
				 		<a href="second-page.php?name=<?php echo $row['name']; ?>"><button type="button" class="link-btn">Second Page</button></a>
				 </div>
			 </div>
		 <?php } ?>
		</div>
	</body>
</html>
