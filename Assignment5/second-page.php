<?php
	$serverName = "localhost";
	$userName = "CPSC2030";
	$password = "CPSC2030";
	$database = "pokedex";

	$conn = mysqli_connect($serverName,$userName,$password,$database);
	if (mysqli_connect_errno($conn)){
  		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>My Second page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet/less" type="text/css" href="style.less">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>
<body>
<div class="header">
	<h1><?php
	$name = mysqli_real_escape_string($conn,$_GET['name']);
	echo strtoupper($name);
	?></h1>
	<a href="first-page.php"><button type="button" class="right">Back to first Page</button></a>
	<?php
		$query="CALL getPokemon(\"$name\")";
		$res = mysqli_query($conn,$query);
	 ?>
</div>
<div class="container">
<ul>
	<?php

	while($fetch = mysqli_fetch_assoc($res)){
		$row = $fetch;
	?>
	<li>NAME:<?php echo $row['name']?></li>
	<li>TYPE:<?php echo $row['type']?></li>
	<li>HP:<?php echo $row['HP']?></li>
	<li>ATK:<?php echo $row['Atk']?></li>
	<li>DEF:<?php echo $row['Def']?></li>
	<li>SAT:<?php echo $row['SAT']?></li>
	<li>SDF:<?php echo $row['SDF']?></li>
	<li>SPD:<?php echo $row['SPD']?></li>
	<li>BST:<?php echo $row['BST']?></li>
	<?php } ?>
</ul>
</div>
</body>
</html>
