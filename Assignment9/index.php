
<html>
	<head>
		<title>Ajax Assignment </title>
		<link href="style.less" type="text/css" rel="stylesheet/less">
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"> </script>
		<script src="jquey.js" defer></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.3/less.min.js"></script>
	</head>
	<body>
		<div class="header">
			<h1>Chat Server</h1>
		</div>
		<div class="container">
			<form method="post" id="form">
				<label>Your Name:</label><br>
				<input type="text" name="userName" id="username" placeholder="Enter Your Name" required><br>
				<label>Message:</label><br>
				<textarea name="message" id="message" placeholder="Enter Your Message" required></textarea>
				<br>
				<input type="button" value="send" id="save" name="sub">
			</form>
		</div>
		<div class="cont">

			<h3 class="item" id="allMsg">Fetch All Messages</h3>
			<p class="fetch_all"></p>
			<h3 class="item" id="last">Last Hour Message</h3>
			<p class="last_hour"></p>

		</div>
	</body>
</html>
