function resClick(event) {
    let menu = $(".menu");
    let realMenu = $(".realMenu");
    let bullet = $(".bullet");
    if (menu.hasClass("open")) {
        menu.removeClass("open");
    } else {
        menu.addClass("open");
    }

    if(realMenu.hasClass("open2")){
      realMenu.removeClass("open2");
    }else{
      realMenu.addClass("open2");
    }

    if(bullet.hasClass("open3")){
      bullet.removeClass("open3");
    }else{
      bullet.addClass("open3");
    }
}

function itemClick(event){
  let bullet = $(".bullet");
  if(bullet.hasClass("float")){
    bullet.removeClass("float");
  }else{
    bullet.addClass("float");
  }
}

$( ".item" ).on( "click", function(event) {
  var url = $(this).attr('href');
  setTimeout("loadPage('https://www.youtube.com/')", 2000);
  event.preventDefault();
});

function loadPage(url){
  window.location.href = url;
}

$(".realMenu ").click(resClick);
$(".item").click(itemClick);
