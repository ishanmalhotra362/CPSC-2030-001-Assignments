var objectArmy=Array(0);
var ctx1 = document.getElementById("photo").getContext("2d");
var ctx2 = document.getElementById("profile").getContext("2d");
var squadArray=Array(0);

var desc1="Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates. ";
var desc2="Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones. ";
var desc3="Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible. ";
var desc4="Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename \"Deadeye Kai.\" Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.";
var desc5="A chipper civilian girl who stumbled upon Squad E through strange circumstances. Nicknamed \"Angie,\" she is beloved by the entire squad for her eagerness to help. She seems to be suffering from amnesia, and can only remember her own name. ";
var desc6="Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals. ";
var desc7="Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household. ";
var desc8="Once a stray, this good good boy is lovingly referred to as \"Rags.\"As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff. ";
var desc9="Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby. ";
var desc10="Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat. ";

class army{
	constructor(name,side,unit,rank,role,desc,image){
		this.name=name;
		this.side=side;
		this.unit=unit;
		this.rank=rank;
		this.role=role;
		this.desc=desc;
		this.image=image;
	}
}

pushElements();

function pushElements(){
	objectArmy.push(new army('Claude Wallace','Edinburgh Army','Ranger Corps, Squad E','First Lieutenant','Tank Commander',desc1,'images/chara01.png'));
	objectArmy.push(new army('Riley Miller','Edinburgh Army','Federate Joint Ops','Second Lieutenant','Artillery Advisor',desc2,'images/chara02.png'));
	objectArmy.push(new army('Raz','Edinburgh Army','Ranger Corps, Squad E','Sergeant','Fireteam Leader',desc3,'images/chara03.png'));
	objectArmy.push(new army('Kai Schulen','Edinburgh Army','Ranger Corps, Squad E','Sergeant Major','Fireteam Leader',desc4,'images/chara04.png'));
	objectArmy.push(new army('Angelica Farnaby','N/A','N/A','N/A','N/A',desc5,'images/chara05.png'));
	objectArmy.push(new army('Minerva Victor','Edinburgh Army','Ranger Corps, Squad F','First Lieutenant','Senior Commander',desc6,'images/chara06.png'));
	objectArmy.push(new army('Karen Stuart','Edinburgh Army','Squad E','Corporal','Combat EMT',desc7,'images/chara07.png'));
	objectArmy.push(new army('Ragnarok','Edinburgh Army','Squad E','K-9 Unit','Mascot',desc8,'images/chara08.png'));
	objectArmy.push(new army('Miles Arbeck','Edinburgh Army','Ranger Corps, Squad E','Sergent','Tank Operator',desc9,'images/chara09.png'));
	objectArmy.push(new army('Dan Bentley','Edinburgh Army','Ranger Corps, Squad E','Private First Class','APC Operator',desc10,'images/chara10.png'));
}

function image(index)
{
		ctx1.clearRect(0, 0, 350,400);
    var armyImage = new Image();
    armyImage.src = objectArmy[index].image;
    armyImage.onload = function ()
    {
        ctx1.drawImage(armyImage, 0, 0, 350, 400);                                                                 //draw image stored in heroImage at xPos and yPos.
    }
		profile(index,objectArmy);
}

function image2(index)
{
		ctx1.clearRect(0, 0, 350,400);
    var armyImage = new Image();
    armyImage.src = squadArray[index].image;
    armyImage.onload = function ()
    {
        ctx1.drawImage(armyImage, 0, 0, 350, 400);                                                                 //draw image stored in heroImage at xPos and yPos.
    }
		profile(index,squadArray);
}

function profile(index,array){
	ctx2.clearRect(0, 0, 300, 400);
	ctx2.font = "15px Comic Sans MS";
	ctx2.fillText("Name:"+array[index].name, 0, 20);
	ctx2.fillText("Side:"+array[index].side, 0, 50);
	ctx2.fillText("Unit:"+array[index].unit, 0, 80);
	ctx2.fillText("Rank:"+array[index].rank, 0, 110);
	ctx2.fillText("Role:"+array[index].role, 0, 140);
	wrapText(array[index].desc,0,170,300,30);
}

function wrapText(text, x, y, maxWidth, lineHeight) {
	var words = text.split(' ');
	var line = 'Description:';

	for(var n = 0; n < words.length; n++) {
		var testLine = line + words[n] + ' ';
		var metrics = ctx2.measureText(testLine);
		var testWidth = metrics.width;
		if (testWidth > maxWidth && n > 0) {
			ctx2.fillText(line, x, y);
			line = words[n] + ' ';
			y += lineHeight;
		}
		else {
			line = testLine;
		}
	}
	ctx2.fillText(line, x, y);
}

function fillArmy(){
	for(var i=0;i<objectArmy.length;i++){
		document.getElementById(""+i).innerHTML=objectArmy[i].name;
	}
}

function fillSquad(index){
	if(squadArray.indexOf(objectArmy[index])<0){
		if(squadArray.length<5){
		squadArray.push(objectArmy[index]);
		}
	}

	for(var i=0;i<squadArray.length;i++)
	{
		document.getElementById("s"+i).innerHTML=squadArray[i].name;
	}

	highlight();
}

function removeSquad(index){
	if(squadArray.length>0){
		squadArray.splice(index, 1);
	}

	for(var i=0;i<squadArray.length;i++)
	{
		document.getElementById("s"+i).innerHTML=squadArray[i].name;
	}

	for(var i=squadArray.length; i<5; i++)
	{
		document.getElementById("s"+i).innerHTML="";
	}
	fillArmy();
	highlight();
}

function highlight(){
	var index=Array(0);
	for(var i=0;i<squadArray.length;i++){
		index.push(objectArmy.indexOf(squadArray[i]));
	}

	for(var i=0;i<index.length;i++){
		document.getElementById(""+index[i]).innerHTML="<b>"+objectArmy[index[i]].name+"</b>";
	}
}
