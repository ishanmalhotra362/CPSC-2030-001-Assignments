-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2018 at 07:12 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllData` ()  select * FROM pokemon$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getNameOnly` (IN `name` VARCHAR(255))  NO SQL
select pokemon.name,pokemon.Nat
from pokemon where name = pokemon.name$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getPokemon` (IN `name` VARCHAR(255))  select * from pokemon where pokemon.name=name$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getPopular` ()  select * from pokemon where name="dialga" OR name="palkia" OR name="heatran" OR name="garchomp" OR name="rampardos" OR name="mamoswine" OR name="rhyperior" OR name="electivire" OR name="regigigas"$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getTypeData` (IN `tpe` VARCHAR(255))  select * from pokemon where type=tpe$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pokemon`
--

CREATE TABLE `pokemon` (
  `id` int(11) NOT NULL,
  `Nat` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `HP` int(11) DEFAULT NULL,
  `Atk` int(11) DEFAULT NULL,
  `Def` int(11) DEFAULT NULL,
  `SAT` int(11) DEFAULT NULL,
  `SDF` int(11) DEFAULT NULL,
  `SPD` int(11) DEFAULT NULL,
  `BST` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon`
--

INSERT INTO `pokemon` (`id`, `Nat`, `name`, `type`, `HP`, `Atk`, `Def`, `SAT`, `SDF`, `SPD`, `BST`) VALUES
(1, 387, 'turtwig', 'Grass', 55, 68, 64, 45, 55, 31, 318),
(2, 388, 'grotle', 'Grass', 75, 89, 85, 55, 65, 36, 405),
(3, 389, 'torterra', 'Grass', 95, 109, 105, 75, 85, 56, 525),
(4, 389, 'torterra', 'Grass', 95, 109, 105, 75, 85, 56, 525),
(5, 390, 'chimchar', 'fire', 44, 58, 44, 58, 44, 61, 309),
(6, 391, 'monferno', 'fire', 64, 78, 52, 78, 52, 81, 405),
(7, 392, 'infernape', 'fire', 76, 104, 71, 104, 71, 108, 534),
(8, 393, 'piplup', 'water', 53, 51, 53, 61, 56, 40, 314),
(9, 394, 'Prinplup', 'water', 64, 66, 68, 81, 76, 50, 405),
(10, 395, 'Empoleon', 'water', 84, 86, 88, 111, 101, 60, 530),
(12, 396, 'Starly', 'normal', 40, 55, 30, 30, 30, 60, 245),
(13, 397, 'Staravia', 'normal', 55, 75, 50, 40, 40, 80, 340),
(14, 398, 'Staraptor', 'normal', 85, 120, 70, 50, 60, 100, 485),
(15, 399, 'Bidoof', 'normal', 59, 45, 40, 35, 40, 31, 250),
(16, 400, 'Bibarel', 'normal', 79, 85, 60, 55, 60, 71, 410),
(17, 401, 'Kricketot', 'bug', 37, 25, 41, 25, 41, 25, 194),
(18, 402, 'Kricketune', 'bug', 77, 85, 51, 55, 51, 65, 384),
(19, 403, 'Shinx', 'electr', 45, 65, 34, 40, 34, 45, 263),
(20, 404, 'Luxio', 'electr', 60, 85, 49, 60, 49, 60, 363),
(21, 405, 'Luxray', 'electr', 80, 120, 79, 95, 79, 70, 523),
(22, 406, 'Budew', 'grass', 40, 30, 35, 50, 70, 55, 280),
(23, 407, 'Roserade', 'grass', 60, 70, 65, 125, 105, 90, 515),
(24, 408, 'Cranidos', 'rock', 67, 125, 40, 30, 30, 58, 350),
(25, 409, 'Rampardos', 'rock', 97, 165, 60, 65, 50, 58, 495),
(26, 410, 'Shieldon', 'rock', 30, 42, 118, 42, 88, 30, 350),
(27, 411, 'Bastiodon', 'rock', 60, 52, 168, 47, 138, 30, 495),
(28, 412, 'Burmy', 'bug', 40, 29, 45, 29, 45, 36, 224),
(29, 413, 'Wormadam', 'bug', 60, 59, 85, 79, 105, 36, 424),
(30, 413, 'Wormadam', 'bug', 60, 79, 105, 59, 85, 36, 424),
(31, 413, 'Wormadam ', 'bug', 60, 69, 95, 69, 95, 36, 424),
(32, 414, 'Mothim', 'bug', 70, 94, 50, 94, 50, 66, 424),
(33, 415, 'Combee', 'bug', 30, 30, 42, 30, 42, 70, 244),
(34, 416, 'Vespiquen', 'bug', 70, 80, 102, 80, 102, 40, 474),
(35, 417, 'Pachirisu', 'electr', 60, 45, 70, 45, 90, 95, 405),
(36, 418, 'Buizel', 'water', 55, 65, 35, 60, 30, 85, 330),
(37, 419, 'Floatzel', 'water', 85, 105, 55, 85, 50, 115, 495),
(38, 420, 'Cherubi', 'grass', 45, 35, 45, 62, 53, 35, 275),
(39, 421, 'Cherrim', 'grass', 70, 60, 70, 87, 78, 85, 450),
(40, 421, 'Cherrim', 'grass', 70, 60, 70, 87, 78, 85, 450),
(41, 422, 'Shellos', 'water', 76, 48, 48, 57, 62, 34, 325),
(42, 423, 'Gastrodon', 'water', 111, 83, 68, 92, 82, 39, 475),
(43, 424, 'Ambipom', 'normal', 75, 100, 66, 60, 66, 115, 482),
(44, 425, 'Drifloon', 'ghost', 90, 50, 34, 60, 44, 70, 348),
(45, 426, 'Drifblim', 'ghost', 150, 80, 44, 90, 54, 80, 498),
(46, 427, 'Buneary', 'normal', 55, 66, 44, 44, 56, 85, 350),
(47, 428, 'Lopunny', 'normal', 65, 76, 84, 54, 96, 105, 480),
(48, 428, 'Mega Lopunny', 'normal', 65, 136, 94, 54, 96, 135, 580),
(49, 429, 'Mismagius', 'ghost', 60, 60, 60, 105, 105, 105, 495),
(50, 430, 'Honchkrow', 'dark', 100, 125, 52, 105, 52, 71, 505),
(51, 431, 'Glameow', 'normal', 49, 55, 42, 42, 37, 85, 310),
(52, 432, 'Purugly', 'normal', 71, 82, 64, 64, 59, 112, 452),
(53, 433, 'Chingling', 'psychc', 45, 30, 50, 65, 50, 45, 285),
(54, 434, 'Stunky', 'poison', 63, 63, 47, 41, 41, 74, 329),
(55, 435, 'Skuntank', 'poison', 103, 93, 67, 71, 61, 84, 479),
(56, 436, 'Bronzor', 'steel', 57, 24, 86, 24, 86, 23, 300),
(57, 437, 'Bronzong', 'steel', 67, 89, 116, 79, 116, 33, 500),
(58, 438, 'Bonsly', 'rock', 50, 80, 95, 10, 45, 10, 290),
(59, 439, 'Mime Jr.', 'psychc', 20, 25, 45, 70, 90, 60, 310),
(60, 440, 'Happiny', 'normal', 100, 5, 5, 15, 65, 30, 220),
(61, 441, 'Chatot', 'normal', 76, 65, 45, 92, 42, 91, 411),
(62, 442, 'Spiritomb', 'ghost', 50, 92, 108, 92, 108, 35, 485),
(63, 443, 'Gible', 'dragon', 58, 70, 45, 40, 45, 42, 300),
(64, 444, 'Gabite', 'dragon', 68, 90, 65, 50, 55, 82, 410),
(65, 445, 'Garchomp', 'dragon', 108, 130, 95, 80, 85, 102, 600),
(66, 445, 'Mega Garchomp', 'dragon', 108, 170, 115, 120, 95, 92, 700),
(67, 446, 'Munchlax', 'normal', 135, 85, 40, 40, 85, 5, 390),
(68, 447, 'Riolu', 'fight', 40, 70, 40, 35, 40, 60, 285),
(69, 448, 'Lucario', 'fight', 70, 110, 70, 115, 70, 90, 525),
(70, 448, 'Mega Lucario', 'fight', 70, 145, 88, 140, 70, 112, 625),
(71, 449, 'hippopotas', 'ground', 68, 72, 78, 38, 42, 32, 330),
(72, 450, 'hippowdon', 'ground', 108, 112, 118, 68, 72, 47, 525),
(73, 451, 'skorupi', 'poison', 40, 50, 90, 30, 55, 65, 330),
(74, 452, 'drapion', 'poison', 70, 90, 110, 60, 75, 95, 500),
(75, 453, 'croagunk', 'poison', 48, 6, 40, 61, 40, 50, 300),
(76, 454, 'toxicroak', 'poison', 83, 106, 65, 86, 65, 85, 490),
(77, 455, 'carnivine', 'grass', 74, 100, 72, 90, 72, 46, 454),
(78, 456, 'finneon', 'water', 49, 49, 56, 49, 61, 66, 330),
(79, 457, 'lumineon', 'water', 69, 69, 76, 69, 86, 91, 460),
(80, 458, 'mantyke', 'water', 45, 20, 50, 60, 120, 50, 345),
(81, 459, 'snover', 'grass', 60, 62, 50, 62, 60, 40, 334),
(82, 460, 'abomasnow', 'grass', 90, 92, 75, 92, 85, 60, 494),
(83, 460, 'mega abomasnow', 'grass', 90, 132, 105, 132, 105, 30, 594),
(84, 461, 'weavile', 'dark', 70, 120, 65, 45, 85, 125, 510),
(85, 462, 'magnezone', 'electr', 70, 70, 115, 130, 90, 60, 535),
(86, 463, 'lickilicky', 'normal', 110, 85, 95, 80, 95, 50, 515),
(87, 464, 'rhyperior', 'ground', 115, 140, 130, 55, 55, 40, 535),
(88, 465, 'tangrowth', 'grass', 100, 100, 125, 110, 50, 50, 535),
(89, 466, 'electivire', 'electr', 75, 123, 67, 95, 85, 95, 540),
(90, 467, 'magmortar', 'fire', 75, 95, 67, 125, 95, 83, 540),
(91, 468, 'togekiss', 'fairy', 85, 50, 95, 120, 115, 80, 545),
(92, 469, 'yanmega', 'bug', 86, 76, 86, 116, 56, 95, 515),
(93, 470, 'leafeon', 'grass', 65, 110, 130, 60, 65, 95, 525),
(94, 471, 'glaceon', 'ice', 65, 60, 110, 130, 95, 65, 525),
(95, 472, 'gliscor', 'ground', 75, 95, 125, 45, 75, 95, 510),
(96, 473, 'mamoswine', 'ice', 110, 130, 80, 70, 60, 80, 530),
(97, 474, 'porygon-Z', 'normal', 85, 80, 70, 135, 75, 90, 535),
(98, 475, 'gallade', 'psychc', 68, 125, 65, 65, 115, 80, 518),
(99, 475, 'mega gallade', 'psychic', 68, 165, 95, 65, 115, 110, 618),
(100, 476, 'probopass', 'rock', 60, 55, 145, 75, 150, 40, 525),
(101, 389, 'torterra', 'Grass', 95, 109, 105, 75, 85, 56, 525),
(102, 389, 'torterra', 'Ground', 95, 109, 105, 75, 85, 56, 525),
(103, 391, 'monferno', 'fight', 64, 78, 52, 78, 52, 81, 405),
(104, 392, 'infernape', 'fight', 76, 104, 71, 104, 71, 108, 534),
(105, 395, 'Empoleon', 'steel', 84, 86, 88, 111, 101, 60, 530),
(106, 396, 'Starly', 'flying', 40, 55, 30, 30, 30, 60, 245),
(107, 397, 'Staravia', 'flying', 55, 75, 50, 40, 40, 80, 340),
(108, 398, 'Staraptor', 'flying', 85, 120, 70, 50, 60, 100, 485),
(109, 400, 'Bibarel', 'water', 79, 85, 60, 55, 60, 71, 410),
(110, 406, 'Budew', 'poison', 40, 30, 35, 50, 70, 55, 280),
(111, 407, 'Roserade', 'poison', 60, 70, 65, 125, 105, 90, 515),
(112, 410, 'Shieldon', 'steel', 30, 42, 118, 42, 88, 30, 350),
(113, 411, 'Bastiodon', 'steel', 60, 52, 168, 47, 138, 30, 495),
(114, 413, 'Wormadam', 'grass', 60, 59, 85, 79, 105, 36, 424),
(115, 413, 'Wormadam', 'ground', 60, 79, 105, 59, 85, 36, 424),
(116, 413, 'Wormadam', 'steel', 60, 69, 95, 69, 95, 36, 424),
(117, 414, 'Mothim', 'flying', 70, 94, 50, 94, 50, 66, 424),
(118, 415, 'Combee', 'flying', 30, 30, 42, 30, 42, 70, 244),
(119, 416, 'Vespiquen', 'flying', 70, 80, 102, 80, 102, 40, 474),
(120, 423, 'Gastrodon', 'ground', 111, 83, 68, 92, 82, 39, 475),
(121, 425, 'Drifloon', 'flying', 90, 50, 34, 60, 44, 70, 348),
(122, 426, 'Drifblim', 'flying', 150, 80, 44, 90, 54, 80, 498),
(123, 428, 'Mega Lopunny', 'fight', 65, 136, 94, 54, 96, 135, 580),
(124, 430, 'Honchkrow', 'flying', 100, 125, 52, 105, 52, 71, 505),
(125, 434, 'Stunky', 'dark', 63, 63, 47, 41, 41, 74, 329),
(126, 435, 'Skuntank', 'dark', 103, 93, 67, 71, 61, 84, 479),
(127, 436, 'Bronzor', 'psychc', 57, 24, 86, 24, 86, 23, 300),
(128, 437, 'Bronzong', 'psychc', 67, 89, 116, 79, 116, 33, 500),
(129, 439, 'Mime Jr.', 'fairy', 20, 25, 45, 70, 90, 60, 310),
(130, 441, 'Chatot', 'flying', 76, 65, 45, 92, 42, 91, 411),
(131, 442, 'Spiritomb', 'dark', 50, 92, 108, 92, 108, 35, 485),
(132, 443, 'Gible', 'ground', 58, 70, 45, 40, 45, 42, 300),
(133, 444, 'Gabite', 'ground', 68, 90, 65, 50, 55, 82, 410),
(134, 445, 'Garchomp', 'ground', 108, 130, 95, 80, 85, 102, 600),
(135, 445, 'Mega Garchomp', 'ground', 108, 170, 115, 120, 95, 92, 700),
(136, 448, 'Lucario', 'steel', 70, 110, 70, 115, 70, 90, 525),
(137, 448, 'Mega Lucario', 'steel', 70, 145, 88, 140, 70, 112, 625),
(138, 451, 'skorupi', 'bug', 40, 50, 90, 30, 55, 65, 330),
(139, 452, 'drapion', 'dark', 70, 90, 110, 60, 75, 95, 500),
(140, 453, 'croagunk', 'fight', 48, 6, 40, 61, 40, 50, 300),
(141, 454, 'toxicroak', 'fight', 83, 106, 65, 86, 65, 85, 490),
(142, 458, 'mantyke', 'flying', 45, 20, 50, 60, 120, 50, 345),
(143, 459, 'snover', 'ice', 60, 62, 50, 62, 60, 40, 334),
(144, 460, 'abomasnow', 'ice', 90, 92, 75, 92, 85, 60, 494),
(145, 460, 'mega abomasnow', 'ice', 90, 132, 105, 132, 105, 30, 594),
(146, 461, 'weavile', 'ice', 70, 120, 65, 45, 85, 125, 510),
(147, 462, 'magnezone', 'steel', 70, 70, 115, 130, 90, 60, 535),
(148, 464, 'rhyperior', 'rock', 115, 140, 130, 55, 55, 40, 535),
(149, 468, 'togekiss', 'flying', 85, 50, 95, 120, 115, 80, 545),
(150, 469, 'yanmega', 'flying', 86, 76, 86, 116, 56, 95, 515),
(151, 472, 'gliscor', 'flying', 75, 95, 125, 45, 75, 95, 510),
(152, 473, 'mamoswine', 'ground', 110, 130, 80, 70, 60, 80, 530),
(153, 475, 'gallade', 'fight', 68, 125, 65, 65, 115, 80, 518),
(154, 475, 'mega gallade', 'fight', 68, 165, 95, 65, 115, 110, 618),
(155, 476, 'probopass', 'steel', 60, 55, 145, 75, 150, 40, 525);

-- --------------------------------------------------------

--
-- Table structure for table `resistantto`
--

CREATE TABLE `resistantto` (
  `type` char(255) DEFAULT NULL,
  `resistantTo` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resistantto`
--

INSERT INTO `resistantto` (`type`, `resistantTo`) VALUES
('normal', 'ghost'),
('fighting', 'rock,bug,dark'),
('flying', 'fighting,ground,bug,grass'),
('poison', 'fighting,poison,grass,fairy'),
('ground', 'poison,rock,electr'),
('rock', 'normal,flying,poison,fire'),
('bug', 'fighting,ground,grass'),
('ghost', 'normal,fighting,poison,bug'),
('steel', 'normal,flying,poison,rock,bug,steel,grass,psychc,ice,dragon,fairy'),
('fire', 'bug,steel,fire,grass,ice'),
('water', 'steel,fire,water,ice'),
('grass', 'ground,water,grass,electr'),
('electr', 'flying,steel,electr'),
('psychc', 'fighting,psychc'),
('ice', 'ice'),
('dragon', 'fire,water,grass,electr'),
('fairy', 'fighting,bug,dragon,dark'),
('dark', 'ghost,psychc,dark');

-- --------------------------------------------------------

--
-- Table structure for table `strongagainst`
--

CREATE TABLE `strongagainst` (
  `type` char(20) DEFAULT NULL,
  `StrongAgainst` char(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `strongagainst`
--

INSERT INTO `strongagainst` (`type`, `StrongAgainst`) VALUES
('normal', ''),
('fighting', 'normal,rock,steel,ice,dark'),
('flying', 'fighting,bug,grass'),
('poison', 'grass,fairy'),
('ground', 'poison,rock,steel,fire,electr'),
('rock', 'flying,bug,fire,ice'),
('bug', 'grass,psychc,dark'),
('ghost', 'ghost,psychc'),
('steel', 'rock,ice,fairy'),
('fire', 'bug,steel,grass,ice'),
('water', 'ground,rock,fire'),
('grass', 'ground,rock,water'),
('electr', 'flying,water'),
('psychc', 'fighting,poison'),
('ice', 'flying,ground,grass,dragon'),
('dragon', 'dragon'),
('fairy', 'fighting,dragon,dark'),
('dark', 'ghost,psychc');

-- --------------------------------------------------------

--
-- Table structure for table `vulnerableto`
--

CREATE TABLE `vulnerableto` (
  `type` char(255) DEFAULT NULL,
  `vulnerableTo` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vulnerableto`
--

INSERT INTO `vulnerableto` (`type`, `vulnerableTo`) VALUES
('normal', 'fighting'),
('fighting', 'flying,psychc,fairy'),
('flying', 'rock,electr,ice'),
('poison', 'ground,psychc'),
('ground', 'water,grass,ice'),
('rock', 'fighting,ground,steel,water,grass'),
('bug', 'flying,rock,fire'),
('ghost', 'ghost,dark'),
('steel', 'fighting,ground,fire'),
('fire', 'ground,rock,water'),
('water', 'grass,electr'),
('grass', 'flying,poison,bug,fire,ice'),
('electr', 'ground'),
('psychc', 'bug,ghost,dark'),
('ice', 'fighting,rock,steel,fire'),
('dragon', 'ice,dragon,fairy'),
('fairy', 'poison,steel'),
('dark', 'fighting,bug,fairy');

-- --------------------------------------------------------

--
-- Table structure for table `weakagainst`
--

CREATE TABLE `weakagainst` (
  `type` char(255) DEFAULT NULL,
  `weakAgainst` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weakagainst`
--

INSERT INTO `weakagainst` (`type`, `weakAgainst`) VALUES
('normal', 'rock,ghost,steel'),
('fighting', 'flying,poison,psychc,bug,ghost,fairy'),
('flying', 'rock,steel,electr'),
('poison', 'poison,ground,rock,ghost,steel'),
('ground', 'flying,bug,grass'),
('rock', 'fighting,ground,steel'),
('bug', 'fighting,flying,poison,ghost,steel,fire,fairy'),
('ghost', 'normal,dark'),
('steel', 'steel,fire,water,electr'),
('fire', 'rock,fire,water,dragon'),
('water', 'water,grass,dragon'),
('grass', 'flying,poison,bug,steel,fire,grass,dragon'),
('electr', 'ground,grass,electr,dragon'),
('psychc', 'steel,psychc,dark'),
('ice', 'steel,fire,water,ice'),
('dragon', 'steel,fairy'),
('fairy', 'poison,steel,fire'),
('dark', 'fighting,dark,fairy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pokemon`
--
ALTER TABLE `pokemon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
