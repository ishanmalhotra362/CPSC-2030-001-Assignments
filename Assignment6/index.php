<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Twigs</title>
</head>
<body>
  <?php
     //We are going to render a template, rather than
     //mix php and html.
     //What this means is that we will
     // -Load the file
     // -store it as a string
     // -Process the string, by replacing templated values
     // -echo the string
     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';  //include the twig library.
     $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

      //Sometimes you have to manually delete the cache
      $twig = new Twig_Environment($loader);

      //Sql setup

     $conn = connectToMyDatabase();
     $query = 'CALL getPopular()';
     $result = mysqli_query($conn,$query);
    ?>
  <div class="grid-container">
    <div class="item1">                <!--heading-->
      <?php
            //setup twig
            $template = $twig->load('title.twig.html');
            //call render to replace values in template with ones specified in my array
            //Since the return value is a string, I can echo it.
            echo $template->render(array("Heading" => "My Favorite Pokemon Page"));
      ?>
    </div>
    <div class="item2">                           <!-- side bar-->
      <?php
          $table = $result->fetch_all(MYSQLI_ASSOC);
          $template1 = $twig->load('menu.twig.html');
          echo $template1->render(array('pokemon' => $table));
          $conn->close();
      ?>
    </div>
    <div class="item3">
      <?php
        $conn_card = connectToMyDatabase();
     if(isset($_GET['name'])){
        session_start();
        $_SESSION['count']=1;
        $_SESSION[0]="";
        $name = mysqli_real_escape_string($conn_card,$_GET['name']);
  /*      for($i=1;$i<=7;$i=$i+1)
        {
            if($_SESSION[$i]=$name)
              break;
            $_SESSION[$i] = $name;
        }
*/
        $qry="CALL getPokemon(\"$name\")";
        $res = mysqli_query($conn_card,$qry);
        $table_poke = $res->fetch_all(MYSQLI_ASSOC);
        $template3 = $twig->load('card.twig.html');
        echo $template3->render(array('allpokemon' => $table_poke));
     }
     $conn_card->close();
      ?>
    </div>
    <div class="item4">                          <!--table-->
      <?php
      $conn1 = connectToMyDatabase();
      $query1 = 'CALL getAllData()';
      $result1 = mysqli_query($conn1,$query1);
      $table1 = $result1->fetch_all(MYSQLI_ASSOC);
      $template2 = $twig->load('table.twig.html');
      echo $template2->render(array('allpokemon' => $table1));
      ?>

      </div>
    </div>
<?php    $conn1->close();       ?>
</div>
<!-- no more html here -->
<!-- leads to cleaner code -->
</body>
</html>
