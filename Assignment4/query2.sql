-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 08:00 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon`
--

CREATE TABLE `pokemon` (
  `id` int(11) NOT NULL,
  `Nat` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `HP` int(11) DEFAULT NULL,
  `Atk` int(11) DEFAULT NULL,
  `Def` int(11) DEFAULT NULL,
  `SAT` int(11) DEFAULT NULL,
  `SDF` int(11) DEFAULT NULL,
  `SPD` int(11) DEFAULT NULL,
  `BST` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon`
--

INSERT INTO `pokemon` (`id`, `Nat`, `name`, `type`, `HP`, `Atk`, `Def`, `SAT`, `SDF`, `SPD`, `BST`, `type`, `vulnerableTo`, `type`, `resistantTo`) VALUES
(5, 390, 'chimchar', 'fire', 44, 58, 44, 58, 44, 61, 309, 'fire', 'ground,rock,water', 'fire', 'bug,steel,fire,grass,ice'),
(19, 403, 'Shinx', 'electr', 45, 65, 34, 40, 34, 45, 263, 'electr', 'ground', 'electr', 'flying,steel,electr'),
(20, 404, 'Luxio', 'electr', 60, 85, 49, 60, 49, 60, 363, 'electr', 'ground', 'electr', 'flying,steel,electr'),
(21, 405, 'Luxray', 'electr', 80, 120, 79, 95, 79, 70, 523, 'electr', 'ground', 'electr', 'flying,steel,electr'),
(35, 417, 'Pachirisu', 'electr', 60, 45, 70, 45, 90, 95, 405, 'electr', 'ground', 'electr', 'flying,steel,electr'),
(89, 466, 'electivire', 'electr', 75, 123, 67, 95, 85, 95, 540, 'electr', 'ground', 'electr', 'flying,steel,electr'),
(90, 467, 'magmortar', 'fire', 75, 95, 67, 125, 95, 83, 540, 'fire', 'ground,rock,water', 'fire', 'bug,steel,fire,grass,ice');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pokemon`
--
ALTER TABLE `pokemon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
