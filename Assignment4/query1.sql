-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 08:05 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon`
--

CREATE TABLE `pokemon` (
  `id` int(11) NOT NULL,
  `Nat` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `HP` int(11) DEFAULT NULL,
  `Atk` int(11) DEFAULT NULL,
  `Def` int(11) DEFAULT NULL,
  `SAT` int(11) DEFAULT NULL,
  `SDF` int(11) DEFAULT NULL,
  `SPD` int(11) DEFAULT NULL,
  `BST` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon`
--

INSERT INTO `pokemon` (`id`, `Nat`, `name`, `type`, `HP`, `Atk`, `Def`, `SAT`, `SDF`, `SPD`, `BST`) VALUES
(48, 428, 'Mega Lopunny', 'normal-fight', 65, 136, 94, 54, 96, 135, 580),
(84, 461, 'weavile', 'dark-ice', 70, 120, 65, 45, 85, 125, 510),
(37, 419, 'Floatzel', 'water', 85, 105, 55, 85, 50, 115, 495),
(43, 424, 'Ambipom', 'normal', 75, 100, 66, 60, 66, 115, 482),
(70, 448, 'Mega Lucario', 'fight-steel', 70, 145, 88, 140, 70, 112, 625),
(52, 432, 'Purugly', 'normal', 71, 82, 64, 64, 59, 112, 452),
(99, 475, 'mega gallade', 'psychic-fight', 68, 165, 95, 65, 115, 110, 618),
(7, 392, 'infernape', 'fire-fight', 76, 104, 71, 104, 71, 108, 534),
(49, 429, 'Mismagius', 'ghost', 60, 60, 60, 105, 105, 105, 495),
(47, 428, 'Lopunny', 'normal', 65, 76, 84, 54, 96, 105, 480),
(65, 445, 'Garchomp', 'dragon-ground', 108, 130, 95, 80, 85, 102, 600),
(14, 398, 'Staraptor', 'normal-flying', 85, 120, 70, 50, 60, 100, 485),
(92, 469, 'yanmega', 'bug-flying', 86, 76, 86, 116, 56, 95, 515),
(95, 472, 'gliscor', 'ground-flying', 75, 95, 125, 45, 75, 95, 510),
(89, 466, 'electivire', 'electr', 75, 123, 67, 95, 85, 95, 540),
(93, 470, 'leafeon', 'grass', 65, 110, 130, 60, 65, 95, 525),
(74, 452, 'drapion', 'poison-dark', 70, 90, 110, 60, 75, 95, 500),
(35, 417, 'Pachirisu', 'electr', 60, 45, 70, 45, 90, 95, 405),
(66, 445, 'Mega Garchomp', 'dragon-ground', 108, 170, 115, 120, 95, 92, 700),
(61, 441, 'Chatot', 'normal-flying', 76, 65, 45, 92, 42, 91, 411),
(79, 457, 'lumineon', 'water', 69, 69, 76, 69, 86, 91, 460),
(97, 474, 'porygon-Z', 'normal', 85, 80, 70, 135, 75, 90, 535),
(69, 448, 'Lucario', 'fight-steel', 70, 110, 70, 115, 70, 90, 525),
(23, 407, 'Roserade', 'grass-poison', 60, 70, 65, 125, 105, 90, 515),
(40, 421, 'Cherrim', 'grass', 70, 60, 70, 87, 78, 85, 450),
(39, 421, 'Cherrim', 'grass', 70, 60, 70, 87, 78, 85, 450),
(36, 418, 'Buizel', 'water', 55, 65, 35, 60, 30, 85, 330),
(51, 431, 'Glameow', 'normal', 49, 55, 42, 42, 37, 85, 310),
(76, 454, 'toxicroak', 'poison-fight', 83, 106, 65, 86, 65, 85, 490),
(46, 427, 'Buneary', 'normal', 55, 66, 44, 44, 56, 85, 350),
(55, 435, 'Skuntank', 'poison-dark', 103, 93, 67, 71, 61, 84, 479),
(90, 467, 'magmortar', 'fire', 75, 95, 67, 125, 95, 83, 540),
(64, 444, 'Gabite', 'dragon-ground', 68, 90, 65, 50, 55, 82, 410),
(6, 391, 'monferno', 'fire-fight', 64, 78, 52, 78, 52, 81, 405),
(98, 475, 'gallade', 'psychc-fight', 68, 125, 65, 65, 115, 80, 518),
(96, 473, 'mamoswine', 'ice-ground', 110, 130, 80, 70, 60, 80, 530),
(13, 397, 'Staravia', 'normal-flying', 55, 75, 50, 40, 40, 80, 340),
(45, 426, 'Drifblim', 'ghost-flying', 150, 80, 44, 90, 54, 80, 498),
(91, 468, 'togekiss', 'fairy-flying', 85, 50, 95, 120, 115, 80, 545),
(54, 434, 'Stunky', 'poison-dark', 63, 63, 47, 41, 41, 74, 329),
(50, 430, 'Honchkrow', 'dark-flying', 100, 125, 52, 105, 52, 71, 505),
(16, 400, 'Bibarel', 'normal-water', 79, 85, 60, 55, 60, 71, 410),
(33, 415, 'Combee', 'bug-flying', 30, 30, 42, 30, 42, 70, 244),
(21, 405, 'Luxray', 'electr', 80, 120, 79, 95, 79, 70, 523),
(44, 425, 'Drifloon', 'ghost-flying', 90, 50, 34, 60, 44, 70, 348),
(32, 414, 'Mothim', 'bug-flying', 70, 94, 50, 94, 50, 66, 424),
(78, 456, 'finneon', 'water', 49, 49, 56, 49, 61, 66, 330),
(94, 471, 'glaceon', 'ice', 65, 60, 110, 130, 95, 65, 525),
(18, 402, 'Kricketune', 'bug', 77, 85, 51, 55, 51, 65, 384),
(73, 451, 'skorupi', 'poison-bug', 40, 50, 90, 30, 55, 65, 330),
(5, 390, 'chimchar', 'fire', 44, 58, 44, 58, 44, 61, 309),
(59, 439, 'Mime Jr.', 'psychc-fairy', 20, 25, 45, 70, 90, 60, 310),
(10, 395, 'Empoleon', 'water-steel', 84, 86, 88, 111, 101, 60, 530),
(12, 396, 'Starly', 'normal-flying', 40, 55, 30, 30, 30, 60, 245),
(85, 462, 'magnezone', 'electr-steel', 70, 70, 115, 130, 90, 60, 535),
(20, 404, 'Luxio', 'electr', 60, 85, 49, 60, 49, 60, 363),
(68, 447, 'Riolu', 'fight', 40, 70, 40, 35, 40, 60, 285),
(82, 460, 'abomasnow', 'grass-ice', 90, 92, 75, 92, 85, 60, 494),
(25, 409, 'Rampardos', 'rock', 97, 165, 60, 65, 50, 58, 495),
(24, 408, 'Cranidos', 'rock', 67, 125, 40, 30, 30, 58, 350),
(3, 389, 'torterra', 'Grass-ground', 95, 109, 105, 75, 85, 56, 525),
(4, 389, 'torterra', 'Grass-ground', 95, 109, 105, 75, 85, 56, 525),
(22, 406, 'Budew', 'grass-poison', 40, 30, 35, 50, 70, 55, 280),
(80, 458, 'mantyke', 'water-flying', 45, 20, 50, 60, 120, 50, 345),
(86, 463, 'lickilicky', 'normal', 110, 85, 95, 80, 95, 50, 515),
(75, 453, 'croagunk', 'poison-fight', 48, 6, 40, 61, 40, 50, 300),
(9, 394, 'Prinplup', 'water', 64, 66, 68, 81, 76, 50, 405),
(88, 465, 'tangrowth', 'grass', 100, 100, 125, 110, 50, 50, 535),
(72, 450, 'hippowdon', 'ground', 108, 112, 118, 68, 72, 47, 525),
(77, 455, 'carnivine', 'grass', 74, 100, 72, 90, 72, 46, 454),
(19, 403, 'Shinx', 'electr', 45, 65, 34, 40, 34, 45, 263),
(53, 433, 'Chingling', 'psychc', 45, 30, 50, 65, 50, 45, 285),
(63, 443, 'Gible', 'dragon-ground', 58, 70, 45, 40, 45, 42, 300),
(87, 464, 'rhyperior', 'ground-rock', 115, 140, 130, 55, 55, 40, 535),
(81, 459, 'snover', 'grass-ice', 60, 62, 50, 62, 60, 40, 334),
(100, 476, 'probopass', 'rock-steel', 60, 55, 145, 75, 150, 40, 525),
(8, 393, 'piplup', 'water', 53, 51, 53, 61, 56, 40, 314),
(34, 416, 'Vespiquen', 'bug-flying', 70, 80, 102, 80, 102, 40, 474),
(42, 423, 'Gastrodon', 'water-ground', 111, 83, 68, 92, 82, 39, 475),
(29, 413, 'Wormadam', 'bug-grass', 60, 59, 85, 79, 105, 36, 424),
(28, 412, 'Burmy', 'bug', 40, 29, 45, 29, 45, 36, 224),
(30, 413, 'Wormadam', 'bug-ground', 60, 79, 105, 59, 85, 36, 424),
(2, 388, 'grotle', 'Grass', 75, 89, 85, 55, 65, 36, 405),
(31, 413, 'Wormadam ', 'bug-steel', 60, 69, 95, 69, 95, 36, 424),
(38, 420, 'Cherubi', 'grass', 45, 35, 45, 62, 53, 35, 275),
(62, 442, 'Spiritomb', 'ghost-dark', 50, 92, 108, 92, 108, 35, 485),
(41, 422, 'Shellos', 'water', 76, 48, 48, 57, 62, 34, 325),
(57, 437, 'Bronzong', 'steel-psychc', 67, 89, 116, 79, 116, 33, 500),
(71, 449, 'hippopotas', 'ground', 68, 72, 78, 38, 42, 32, 330),
(1, 387, 'turtwig', 'Grass', 55, 68, 64, 45, 55, 31, 318),
(15, 399, 'Bidoof', 'normal', 59, 45, 40, 35, 40, 31, 250),
(60, 440, 'Happiny', 'normal', 100, 5, 5, 15, 65, 30, 220),
(26, 410, 'Shieldon', 'rock-steel', 30, 42, 118, 42, 88, 30, 350),
(27, 411, 'Bastiodon', 'rock-steel', 60, 52, 168, 47, 138, 30, 495),
(83, 460, 'mega abomasnow', 'grass-ice', 90, 132, 105, 132, 105, 30, 594),
(17, 401, 'Kricketot', 'bug', 37, 25, 41, 25, 41, 25, 194),
(56, 436, 'Bronzor', 'steel-psychc', 57, 24, 86, 24, 86, 23, 300),
(58, 438, 'Bonsly', 'rock', 50, 80, 95, 10, 45, 10, 290),
(67, 446, 'Munchlax', 'normal', 135, 85, 40, 40, 85, 5, 390);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pokemon`
--
ALTER TABLE `pokemon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
